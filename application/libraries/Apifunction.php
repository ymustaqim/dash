<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Apifunction {

    public function url_api() {
        return "http://pmia.morbis.id";
    }

    public function minute_to_hour($minute){
        return floor($minute/60);
    }

    public function hour_to_day($minute){
        return floor($minute/24);
    }
}