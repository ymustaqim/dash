 <?php 
 $username = ($this->session->userdata['logged_in']['user_username']); 
// $photo = ($this->session->userdata['logged_in']['photo']); 

 ?>

 <!-- ============================================================== -->
 <!-- Left Sidebar - style you can find in sidebar.scss  -->
 <!-- ============================================================== -->
 <div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav slimscrollsidebar">
        <div class="sidebar-head">
            <h3><span class="fa-fw open-close"><i class="ti-close ti-menu"></i></span> <span class="hide-menu">Navigation</span></h3> </div>
            <div class="user-profile">
                <div class="dropdown user-pro-body">
                  <div><img src="<?php echo base_url();?>assets/admin/plugins/images/users/varun.jpg" alt="user-img" class="img-circle"></div>
                  <a href="#" class="dropdown-toggle u-dropdown" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $username; ?> <span class="caret"></span></a>
                  <ul class="dropdown-menu animated flipInY">
                    <li><a href="<?php echo base_url();?>profile/index"><i class="ti-user"></i> My Profile</a></li>
                </ul>
            </div>
        </div>
        <ul class="nav" id="side-menu">

            <li>
                <a href="<?php echo base_url();?>dashboard" class="waves-effect">
                <i class="mdi mdi-av-timer fa-fw" data-icon="v"></i>
                <span class="hide-menu"> Dashboard </span>
                </a>
            </li>
            <hr>

            <li>
                <a href="#" class="waves-effect">
                <i class="mdi mdi-account-circle fa-fw"></i>
                <span class="hide-menu">Login and Register<span class="fa arrow"></span></span>
                </a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="<?php echo base_url();?>docs/login_app/register" class="waves-effect"><i class="mdi mdi-account-edit fa-fw"></i> <span class="hide-menu">Register</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>docs/login_app/index" class="waves-effect"><i class="mdi mdi-login-variant fa-fw"></i> <span class="hide-menu">Login</span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url();?>docs/login_app/password" class="waves-effect"><i class="mdi mdi-lock fa-fw"></i> <span class="hide-menu">Password</span></a>
                    </li>
                </ul>
            </li>
        </ul>
    </div>
</div>
<!-- ============================================================== -->
<!-- End Left Sidebar -->
        <!-- ============================================================== -->