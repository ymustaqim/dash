<?php $this->load->view("apidocs/template/header.php") ?>
<?php $this->load->view("apidocs/template/sidebar.php") ?>
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
	<div class="container-fluid">
		<div class="row bg-title">
			<div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
				<h4 class="page-title">Dashboard 1</h4> </div>
				<div class="col-lg-9 col-sm-8 col-md-8 col-xs-12"> <button class="right-side-toggle waves-effect waves-light btn-info btn-circle pull-right m-l-20"><i class="ti-settings text-white"></i></button>
					<!-- <a href="#" target="_blank" class="btn btn-danger pull-right m-l-20 hidden-xs hidden-sm waves-effect waves-light">Buy Admin Now</a> -->
					<ol class="breadcrumb">
						<li class="active">Dashboard</li>
					</ol>
				</div>
				<!-- /.col-lg-12 -->
			</div>
			<!-- /.row -->
			<!-- ============================================================== -->
			<!-- Different data widgets -->
			<!-- ============================================================== -->
			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="panel panel-default">
                            <div class="panel-heading">Selamat datang</div>
                            <div class="panel-wrapper collapse in">
                                <div class="panel-body">
                                    <p>Selamat datang dihalaman DASH API</p> <a href="<?php echo base_url();?>docs/login_app/register" class="btn btn-custom m-t-10 collapseble">Get Code</a>
                                </div>
                                <!-- <div class="panel-footer"> Panel Footer </div> -->
                            </div>
                        </div>
                    </div>

		</div>
		<!-- /.container-fluid -->
		<footer class="footer text-center"> <?php echo date('Y') ?> &copy; DASH App </footer>
	</div>
	<!-- ============================================================== -->
	<!-- End Page Content -->
	<!-- ============================================================== -->
	<?php $this->load->view("apidocs/template/footer.php") ?>