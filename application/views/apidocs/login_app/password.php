<?php $this->load->view("apidocs/template/header.php") ?>
<?php $this->load->view("apidocs/template/sidebar.php") ?>

<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Password</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Password</li>
                    </ol>
                </div>
            </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <strong>(PUT) Reset Password</strong> 
                        <button data-toggle="collapse" data-target="#reset_password" aria-expanded="true" aria-controls="collapseExample" class="btn btn-info btn-xs pull-right"><i class="glyphicon glyphicon-menu-down"></i>
                        </button>
                    </div> 
                    <div id="reset_password" class="panel-body collapse in" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="<?= $this->apifunction->url_api();?>/api/pmi/Pengguna_app/reset_password" name="" class="form-control">
                        </div> 
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Header</th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <th colspan="3" class="text-center">-</th>
                                    </tr>
                                </tbody> 
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Parameter</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>username<span class="required-parameter">*</span></td> 
                                        <td>Nomor Telepon Pengguna</td> 
                                        <td>087228127383</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                            <span class="required-parameter"><i>*required parameter</i></span>
                            </p>
                        </div> 
<strong>Response Success: </strong>
<pre>
{
    "status": true,
    "message": "Berhasil melakukan reset password username 087228127383,  Password akan dikirim lewat SMS.",
    "data": [
        {
            "username": "087228127383",
            "password": "$2y$12$HTHApPXMMekV3mm7pKYAqea5o7aHQ.Ikfq0lxmCCW7XBgnEouGkIS",
            "key": "wooo0cw4o4oo8g04g0448g08sog848wkcs8ggcgs"
        }
    ]
}
</pre> 


<hr>
<strong>Response  Username salah atau tidak ditemukan:</strong> 
<pre>
{
    "status": false,
    "message": "Mohon maaf, username 087228127383 belum terdaftar. Silahkan periksa kembali username anda.",
    "data": []
}
</pre> 
    </div>
</div>
 <!-- END Panel INFO -->


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <strong>(PUT) Change Password</strong> 
                        <button data-toggle="collapse" data-target="#change_password" aria-expanded="true" aria-controls="collapseExample" class="btn btn-info btn-xs pull-right"><i class="glyphicon glyphicon-menu-down"></i>
                        </button>
                    </div> 
                    <div id="change_password" class="panel-body collapse in" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="<?= $this->apifunction->url_api();?>/api/pmi/Pengguna_app/change_password" name="" class="form-control">
                        </div> 
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Header</th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="1">Authorization</td>
                                        <td colspan="2">Basic Auth {username : username, password : password}</td>
                                    </tr>
                                </tbody> 
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Parameter</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>MBI-PMI-API-KEY<span class="required-parameter">*</span></td>
                                        <td>API KEY yang valid untuk mengakses endpoint</td>
                                        <td>88ck0g8og0cwk4sgwws88cggk80wgc84kko4ks8a</td>
                                    </tr>
                                    <tr>
                                        <td>id_pengguna_app<span class="required-parameter">*</span></td> 
                                        <td>ID dari pengguna applikasi</td> 
                                        <td>14</td>
                                    </tr>
                                    <tr>
                                        <td>old_password<span class="required-parameter">*</span></td> 
                                        <td>Password lama pengguna</td> 
                                        <td>7383</td>
                                    </tr>
                                    <tr>
                                        <td>new_password<span class="required-parameter">*</span></td> 
                                        <td>Password baru pengguna</td> 
                                        <td>8227</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                            <span class="required-parameter"><i>*required parameter</i></span>
                            </p>
                        </div> 
<strong>Response Success: </strong>
<pre>
{
    "status": true,
    "message": "Password berhasil diganti",
    "data": []
}
</pre> 


<hr>
<strong>Response Password Lama Tidak sesuai:</strong> 
<pre>
{
    "status": false,
    "message": "Password lama anda tidak sesuai, mohon periksa kembali.",
    "data": []
}
</pre> 
    </div>
</div> 
 <!-- END Panel INFO -->


</div>
</div>
</div>
</div>
</div>


<!-- /#page-wrapper -->
<?php $this->load->view("apidocs/template/footer.php");
?>