<?php $this->load->view("apidocs/template/header.php") ?>
<?php $this->load->view("apidocs/template/sidebar.php") ?>

<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Login</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Login</li>
                    </ol>
                </div>
            </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <strong>(POST) Login</strong> 
                        <button data-toggle="collapse" data-target="#store-apointment" aria-expanded="true" aria-controls="collapseExample" class="btn btn-warning btn-xs pull-right"><i class="glyphicon glyphicon-menu-down"></i>
                        </button>
                    </div> 
                    <div id="store-apointment" class="panel-body collapse in" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="<?= $this->apifunction->url_api();?>/api/pmi/Pengguna_app/login" name="" class="form-control">
                        </div> 
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Header</th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <th colspan="3" class="text-center">-</th>
                                    </tr>
                                </tbody> 
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Parameter</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>username<span class="required-parameter">*</span></td> 
                                        <td>Nomor Telepon Pengguna</td> 
                                        <td>087228127383</td>
                                    </tr>
                                    <tr>
                                        <td>password<span class="required-parameter">*</span></td> 
                                        <td>Password pengguna</td> 
                                        <td>8761</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                            <span class="required-parameter"><i>*required parameter</i></span>
                            </p>
                        </div> 
<strong>Response Success: </strong> 
<pre>
{
    "status": true,
    "message": "Login Berhasil",
    "data": [
        {
            "id_pengguna_app": "11",
            "username": "087228127383",
            "password": "$2y$12$W6mOj1bjIqgy5JIU7CmUde14CXjPx76FEgbz6Dy.uP40frxJDZfK2",
            "key": "owccokkooc0kw4ko444c4c4o04sg8skgg0000kcc"
        }
    ]
}
</pre> 


<hr>
<strong>Response Username salah atau tidak ditemukan:</strong> 
<pre>
{
    "status": false,
    "message": "Username 087228127383 tidak ditemukan, silahkan melakukan registrasi terlebih dahulu",
    "data": []
}
</pre> 

<hr>
<strong>Response Password salah </strong> 
<pre>
{
    "status": false,
    "message": "Login Gagal. Password anda salah ",
    "data": []
}
</pre>
    </div>
</div> 


</div>
</div>
</div>
</div>
</div>


<!-- /#page-wrapper -->
<?php $this->load->view("apidocs/template/footer.php");
?>