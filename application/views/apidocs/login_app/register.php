<?php $this->load->view("apidocs/template/header.php") ?>
<?php $this->load->view("apidocs/template/sidebar.php") ?>

<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Register</h4> </div>
                <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                    <ol class="breadcrumb">
                        <li><a href="#">Dashboard</a></li>
                        <li class="active">Register</li>
                    </ol>
                </div>
            </div>
    <div class="row">
        <div class="col-md-12">
            <div class="panel-group">
                <div class="panel panel-warning">
                    <div class="panel-heading">
                        <strong>(POST) Register</strong> 
                        <button data-toggle="collapse" data-target="#store-apointment" aria-expanded="true" aria-controls="collapseExample" class="btn btn-warning btn-xs pull-right"><i class="glyphicon glyphicon-menu-down"></i>
                        </button>
                    </div> 
                    <div id="store-apointment" class="panel-body collapse in" aria-expanded="true" style="">
                        <div class="form-group">
                            <label>URL</label> 
                            <input type="text" disabled="disabled" value="<?= $this->apifunction->url_api();?>/api/pmi/Pengguna_app/register" name="" class="form-control">
                        </div> 
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered">
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Header</th>
                                    </tr> 
                                </thead>
                                <tbody>
                                    <tr>
                                        <th colspan="3" class="text-center">-</th>
                                    </tr>
                                </tbody> 
                                <thead>
                                    <tr>
                                        <th colspan="3" class="text-center">Parameter</th>
                                    </tr>
                                </thead>
                                <thead>
                                    <tr>
                                        <th>Parameter</th> 
                                        <th>Deskripsi</th> 
                                        <th>Contoh Nilai</th>
                                    </tr>
                                </thead> 
                                <tbody>
                                    <tr>
                                        <td>nama<span class="required-parameter">*</span></td> 
                                        <td>Nama Pengguna</td> 
                                        <td>Morbis</td>
                                    </tr>
                                    <tr>
                                        <td>no_hp<span class="required-parameter">*</span></td> 
                                        <td>Nomor Telepon Pengguna. Saat Registrasi Username diambil dari Nomor Telepon</td> 
                                        <td>087228127383</td>
                                    </tr>
                                </tbody>
                            </table>

                            <p>
                            <span class="required-parameter"><i>*required parameter</i></span>
                            </p>
                        </div> 
<strong>Response Success: </strong>
<pre>
{
    "status": true,
    "message": "Berhasil melakukan register dengan username 087228127383. Password akan dikirim lewat SMS",
    "data": [
        {
            "username": "087228127383",
            "password": "$2y$12$iEPasoeQH1ZSYAPAOB8x9exgthefj3VQNwcr2mnJLHkn6vrLS3n4a",
            "key": "cocco8wogo4w04o4s8ksos4g0wosgk8oowcg0ss4"
        }
    ]
}
</pre> 


<hr>
<strong>Response nomor telepon sudah digunakan:</strong> 
<pre>
{
    "status": false,
    "message": "Mohon maaf, Nomor 087228127383 telah digunakan. Silahkan menggunakan Nomor yang lain",
    "data": []
}
</pre> 
    </div>
</div> 


</div>
</div>
</div>
</div>
</div>


<!-- /#page-wrapper -->
<?php $this->load->view("apidocs/template/footer.php");
?>