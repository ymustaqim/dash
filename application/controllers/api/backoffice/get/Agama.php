<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Agama extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_agama');

    }

    function agama_get(){
      //untuk ambil detail data, kasih id disini
      $id = $this->get('id_agama');

      if ($id) {
        $data = $this->Model_agama->get_data_agama_detail($id);
      } else {
        $data = $this->Model_agama->get_data_agama();
      }
      
      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List Agama.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}