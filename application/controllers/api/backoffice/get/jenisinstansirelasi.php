<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class jenisinstansirelasi extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_jenisinstansirelasi');

    }

    function jenisinstansirelasi_get(){
      $data = $this->Model_jenisinstansirelasi->get_data_jenisinstansirelasi();
      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List Jenis Instansi Relasi.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}
