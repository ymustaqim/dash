<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Icd10 extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_icd10');

    }

    function icd10_get(){
      //untuk ambil detail data, kasih id disini
      $id = $this->get('id_icd_10');

      if ($id) {
        $data = $this->Model_icd10->get_data_icd10_detail($id);
      } else {
        $data = $this->Model_icd10->get_data_icd10();
      }
      
      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List icd 10.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}