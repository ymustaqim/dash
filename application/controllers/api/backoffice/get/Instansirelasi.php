<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Instansirelasi extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_instansirelasi');

    }

    function instansirelasi_get(){
      $data = $this->Model_instansirelasi->get_data_instansirelasi();
      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List Instansi Relasi.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}
