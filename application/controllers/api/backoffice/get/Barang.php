<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Barang extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_barang');

    }

    function barang_get(){
      //untuk ambil detail data, kasih id disini
      $id = $this->get('id_barang');

      if ($id) {
        $data = $this->Model_barang->get_data_barang_detail($id);
      } else {
        $data = $this->Model_barang->get_data_barang();
      }
      
      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List barang.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}