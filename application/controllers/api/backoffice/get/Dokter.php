<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Dokter extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_dokter');

    }

    function dokter_get(){
      //untuk ambil detail data, kasih id disini
      $id = $this->get('id_dokter');
    
      if ($id) {
        $data = $this->Model_dokter->get_data_dokter_detail($id);
      } else {
        $data = $this->Model_dokter->get_data_dokter();
      }
    

      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List dokter.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}