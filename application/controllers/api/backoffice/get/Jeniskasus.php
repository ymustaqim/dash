<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Jeniskasus extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_jenis_kasus');

    }

    function jeniskasus_get(){
      $data = $this->Model_jenis_kasus->get_data_jeniskasus();
      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List Jenis Kasus.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}
