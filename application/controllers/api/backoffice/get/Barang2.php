<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Barang2 extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_barang');

    }

    function barang_get(){
      //untuk ambil detail data, kasih id disini
      $data=$this->Model_barang->get_data_barang_detail();
      
      if ($data) {
        $this->response([
          'status'  => TRUE,
          'message' => 'List barang.',
          'barang'  => $data
      ], REST_Controller::HTTP_OK);
    }
}

}