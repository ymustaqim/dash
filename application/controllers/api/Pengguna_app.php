<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Pengguna_app extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_pengguna_app');
        $this->load->helper('string');
    }
    
    private function _generate_key(){
        do {
            // Generate a random salt
            $salt = base_convert(bin2hex($this->security->get_random_bytes(64)), 16, 36);
            // If an error occurred, then fall back to the previous method
            if ($salt === false)
            {
                $salt = hash('sha256', time() . mt_rand());
            }
            $new_key = substr($salt, 0, config_item('rest_key_length'));
        }
        while ($this->_key_exists($new_key));
        return $new_key;
    }

    private function _key_exists($key){
        return $this->rest->db
            ->where(config_item('rest_key_column'), $key)
            ->count_all_results(config_item('rest_keys_table')) > 0;
    }

    function index_get() {
        $id = $this->get('id');
        $data = $this->Model_pengguna_app->get($id);
 
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data Pengguna App berhasil didapatkan',
                'data' => $data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data Pengguna App gagal didapatkan',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }
    
    function index_post(){
        $username = $this->post('username');
        $pengguna_app = $this->Model_pengguna_app->get_by_username($username);
        if(!$pengguna_app){
            $this->response([
                'status' => FALSE,
                'message' => 'Username '.$username.' telah digunakan, silahkan gunakan username lain.',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
        $api_key = null;
        $data = [
            'username' => $this->post('username'),
            'password' => $this->post('password'),
            'api_key' => $api_key,
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $inserted_data = $this->Model_pengguna_app->insert($data);
 
        if ($inserted_data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data Pengguna App berhasil dimasukkan',
                'data' => $inserted_data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data Pengguna App gagal dimasukkan',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }

    function index_put(){
        $id = $this->put('id');
        $data = [
            'keterangan' => $this->put('keterangan'),
            'id_pmi' => $this->put('id_pmi'),
            'penyelenggara' => $this->put('penyelenggara'),
            'tgl_acara' => $this->put('tgl_acara'),
            'rencana_peserta' => $this->put('rencana_peserta'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        $updated_data = $this->Model_pengguna_app->update($id, $data);
 
        if ($updated_data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data Pengguna App berhasil diubah',
                'data' => $updated_data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data Pengguna App gagal diubah',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }

    function index_delete(){
        $id = $this->delete('id');
        $deleted_data = $this->Model_pengguna_app->delete($id);
        
        if ($deleted_data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data Pengguna App berhasil dihapus',
                'data' => $deleted_data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data Pengguna App gagal dihapus',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }

    function login_post(){
        $username = $this->post('username');
        $pengguna_app = $this->Model_pengguna_app->get_by_username($username);
        if($pengguna_app){
            $data = [
                'username' => $this->post('username'),
                'password' => $this->post('password'),
            ];
            $user_login = $this->Model_pengguna_app->login($data);
            if($user_login){
                $this->response([
                    'status' => TRUE,
                    'message' => 'Login Berhasil',
                    'data' => $user_login
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Login Gagal. Password anda salah ',
                    'data' => []
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Username '.$username.' tidak ditemukan, silahkan melakukan registrasi terlebih dahulu',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }

    function register_post(){
        $nama = $this->post('nama');
        $no_hp = $this->post('no_hp');
        
        $pengguna_app = $this->Model_pengguna_app->get_by_no_hp($no_hp);
        if($pengguna_app){
            $this->response([
                'status' => FALSE,
                'message' => 'Mohon maaf, Nomor '.$no_hp.' telah digunakan. Silahkan menggunakan Nomor yang lain',
                'data' => []
            ], REST_Controller::HTTP_OK);
        } else {
            // SMS OTP
            $phone = $no_hp;
            $api_key = "3b22dcb855fc7d2441a57d85ea91a759"; //key from ayo SMS
            $trans_id   = date('this');

            $firstNumber = substr($phone,0,1);
            if ($firstNumber != 0) {
                $send_to = '0'.$phone;
                $phone_number = '0'.$phone;
            } else {
                $send_to = $phone;
                $phone_number = $phone;
            }
            // $send_to    = (substr($phone,0,1) == 0) ? '62'.substr($phone, 1, strlen($phone)) : $phone;

            $auto_pass  = random_string('numeric', 4);
            $message    = urlencode('Password PMI App anda adalah '.$auto_pass.'. Mohon rahasiakan demi keamanan data anda.');
            $namaFrom = urlencode('SMS BISNIS');

            $curl = curl_init();
            $url_sms = "https://api.ayosms.com/mconnect/gw/sendsms.php?api_key=$api_key&from=$namaFrom&to=$send_to&msg=$message&trx_id=$trans_id";
            curl_setopt_array($curl, array(
                CURLOPT_URL            => $url_sms,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST"
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            //END SMS OTP

            if ($err) {
                $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'Error. Gagal untuk mengirimkan SMS. Silakan coba lagi.',
                    'data'      => array()
                ), Rest::HTTP_OK);

            } else {

                //Cek table penduduk. Kalau belum ada, insert ke penduduk dulu.
                $penduduk_db = $this->Model_pengguna_app->get_penduduk_by_no_hp($no_hp);
                if(!$penduduk_db){
                    $data_penduduk = [
                        'nama' => $nama,
                        'no_hp' => $no_hp,
                        'created_at' => date('Y-m-d H:i:s'),
                    ];
                    $penduduk_db = $this->Model_pengguna_app->insert_penduduk($data_penduduk);
                }

                $options = [
                    'cost' => 12,
                ];
                $password_hashed = password_hash($auto_pass, PASSWORD_BCRYPT, $options);
                
                $data_pengguna = [
                    'id_penduduk' => $penduduk_db[0]['id'],
                    'nama' => $this->post('nama'),
                    'username' => $this->post('no_hp'),
                    'password' => $password_hashed,
                    'no_hp' => $this->post('no_hp'),
                    'created_at' => date('Y-m-d H:i:s'),
                ];
                $registered_pengguna_app = $this->Model_pengguna_app->insert($data_pengguna);
                if($registered_pengguna_app){
                    $key = $this->_generate_key();
                    $data_key = [
                        'pengguna_id' => $registered_pengguna_app[0]['id'],
                        'key' => $key,
                    ];
                    $inserted_key = $this->Model_pengguna_app->insert_key($data_key);
                    if($inserted_key){
                        $pengguna_app_key  = $this->Model_pengguna_app->get_with_key($inserted_key[0]['pengguna_id']);
                        $this->response([
                            'status' => TRUE,
                            'message' => 'Berhasil melakukan register dengan nomer '.$pengguna_app_key[0]['no_hp']. ',  Password akan dikirim lewat SMS.',
                            'data' => $pengguna_app_key
                        ], REST_Controller::HTTP_OK);

                    } else {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Mohon maaf, Proses register gagal, silahkan ulangi kembali',
                            'data' => "Gagal Generate Key"
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Mohon maaf, Proses register gagal, silahkan ulangi kembali',
                        'data' => "Gagal Mendaftarkan Pengguna App"
                    ], REST_Controller::HTTP_OK);
                }
            }
        }
    }

    function reset_password_put(){
        $username = $this->put('username');
        
        $pengguna_app = $this->Model_pengguna_app->get_by_username($username);
        if(!$pengguna_app){
            $this->response([
                'status' => FALSE,
                'message' => 'Mohon maaf, username '.$username.' belum terdaftar. Silahkan periksa kembali username anda.',
                'data' => []
            ], REST_Controller::HTTP_OK);
        } else {
            // SMS OTP
            $phone = $username;
            $api_key = "3b22dcb855fc7d2441a57d85ea91a759"; //key from ayo SMS
            $trans_id   = date('this');

            $firstNumber = substr($phone,0,1);
            if ($firstNumber != 0) {
                $send_to = '0'.$phone;
                $phone_number = '0'.$phone;
            } else {
                $send_to = $phone;
                $phone_number = $phone;
            }
            // $send_to    = (substr($phone,0,1) == 0) ? '62'.substr($phone, 1, strlen($phone)) : $phone;

            $auto_pass  = random_string('numeric', 4);
            $message    = urlencode('Password PMI App anda adalah '.$auto_pass.'. Mohon rahasiakan demi keamanan data anda.');
            $namaFrom = urlencode('SMS BISNIS');

            $curl = curl_init();
            $url_sms = "https://api.ayosms.com/mconnect/gw/sendsms.php?api_key=$api_key&from=$namaFrom&to=$send_to&msg=$message&trx_id=$trans_id";
            curl_setopt_array($curl, array(
                CURLOPT_URL            => $url_sms,
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING       => "",
                CURLOPT_MAXREDIRS      => 10,
                CURLOPT_TIMEOUT        => 30,
                CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST  => "POST"
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);
            //END SMS OTP

            if ($err) {
                $this->response(array(
                    'status'    => FALSE,
                    'message'   => 'Error. Gagal untuk mengirimkan SMS. Silakan coba lagi.',
                    'data'      => array()
                ), Rest::HTTP_OK);

            } else {
                $options = [
                    'cost' => 12,
                ];
                $password_hashed = password_hash($auto_pass, PASSWORD_BCRYPT, $options);

                $data_pengguna = [
                    'password' => $password_hashed,
                    'updated_at' => date('Y-m-d H:i:s'),
                ];
                $pengguna_app = $this->Model_pengguna_app->update($pengguna_app[0]['id'],$data_pengguna);
                if($pengguna_app){
                    $pengguna_app_key  = $this->Model_pengguna_app->get_with_key($pengguna_app[0]['id']);
                    if($pengguna_app_key){
                        $this->response([
                            'status' => TRUE,
                            'message' => 'Berhasil melakukan reset password username '.$pengguna_app_key[0]['username']. ',  Password akan dikirim lewat SMS.',
                            'data' => []
                            // 'data' => $pengguna_app_key
                        ], REST_Controller::HTTP_OK);
                    } else {
                        $this->response([
                            'status' => FALSE,
                            'message' => 'Gagal Mendapatkan API KEY',
                            'data' => []
                        ], REST_Controller::HTTP_OK);
                    }
                } else {
                    $this->response([
                        'status' => FALSE,
                        'message' => 'Mohon maaf, Proses reset password gagal, silahkan ulangi kembali',
                        'data' => "Gagal Update Password Pengguna App"
                    ], REST_Controller::HTTP_OK);
                }
            }
        }
    }

    function change_password_put(){
        $id_pengguna_app = $this->put('id_pengguna_app');
        $old_password = $this->put('old_password');
        $new_password = $this->put('new_password');
        $options = [
            'cost' => 12,
        ];
        $password_hashed = password_hash($new_password, PASSWORD_BCRYPT, $options);
        
        $pengguna_app = $this->Model_pengguna_app->get($id_pengguna_app);
        if(password_verify($old_password,$pengguna_app[0]['password'])){
            $data = [
                'password' => $password_hashed,
                'updated_at' => date('Y-m-d H:i:s'),
            ];
            $updated_data = $this->Model_pengguna_app->update($id_pengguna_app, $data);
            if($updated_data){
                $this->response([
                    'status' => TRUE,
                    'message' => 'Password berhasil diganti',
                    'data' => []
                ], REST_Controller::HTTP_OK);
            } else {
                $this->response([
                    'status' => FALSE,
                    'message' => 'Password gagal diganti',
                    'data' => []
                ], REST_Controller::HTTP_OK);
            }
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Password lama anda tidak sesuai, mohon periksa kembali.',
                'data' => []
            ], REST_Controller::HTTP_OK);
        }
        
    }
    
    function profile_get(){
        $id_pengguna = $this->get('id_pengguna');
        $data = $this->Model_pengguna_app->get_profile($id_pengguna);
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Profile berhasil ditemukan',
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Profile gagal ditemukan',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }       
    }

    function voucher_get(){
        $id_pengguna = $this->get('id_pengguna');
        $data = $this->Model_pengguna_app->get_voucher($id_pengguna);
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Voucher berhasil ditemukan',
                'data' => $data
            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Voucher tidak ditemukan',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }     
    }

    function profile_put(){
        // Note :
        // - Nomor Telepon tidak dapat diganti (disable)
        // - Tidak ada penyuntingan tanggal lahir
        // - ID Kelurahan masih belum digunakan

        $id_pengguna_app = $this->put('id_pengguna');
        $pengguna_app = $this->Model_pengguna_app->get($id_pengguna_app);
        $data = [
            'nama' => $this->put('nama'),
            'no_ktp' => $this->put('no_ktp'),
            'alamat' => $this->put('alamat'),
            'email' => $this->put('email'),
            'id_golongan_darah' => $this->put('id_golongan_darah'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        
        $profile = $this->Model_pengguna_app->update_penduduk($pengguna_app[0]['id_penduduk'],$data);

        if($profile){
            $this->response([
                'status' => TRUE,
                'message' => 'Profile berhasil diganti',
                'data' => $profile
            ], REST_Controller::HTTP_OK);
        } else {
            $this->response([
                'status' => FALSE,
                'message' => 'Profile gagal diganti',
                'data' => []
            ], REST_Controller::HTTP_OK);
        }
    }
    

}