<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Pmi extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->model('Model_pmi');
    }

    function index_get() {
        $id = $this->get('id');
        $data = $this->Model_pmi->get($id);
 
        if ($data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data PMI berhasil didapatkan',
                'data' => $data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data PMI gagal didapatkan',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }
    
    function index_post(){
        date_default_timezone_set("Asia/Jakarta");
        $data = [
            'id_kelurahan' => $this->post('id_kelurahan'),
            'id_user_input' => $this->post('id_user_input'),
            'nama_pmi' => $this->post('nama_pmi'),
            'alamat' => $this->post('alamat'),
            'no_hp' => $this->post('no_hp'),
            'email' => $this->post('email'),
            'latitude' => $this->post('latitude'),
            'longitude' => $this->post('longitude'),
            'created_at' => date('Y-m-d H:i:s'),
        ];
        $inserted_data = $this->Model_pmi->insert($data);
 
        if ($inserted_data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data PMI berhasil dimasukkan',
                'data' => $inserted_data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data PMI gagal dimasukkan',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }

    function index_put(){
        date_default_timezone_set("Asia/Jakarta");

        $id = $this->put('id');
        $data = [
            'id_kelurahan' => $this->put('id_kelurahan'),
            'id_user_input' => $this->put('id_user_input'),
            'nama_pmi' => $this->put('nama_pmi'),
            'alamat' => $this->put('alamat'),
            'no_hp' => $this->put('no_hp'),
            'email' => $this->put('email'),
            'latitude' => $this->put('latitude'),
            'longitude' => $this->put('longitude'),
            'updated_at' => date('Y-m-d H:i:s'),
        ];
        
        $updated_data = $this->Model_pmi->update($id, $data);
 
        if ($updated_data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data PMI berhasil diubah',
                'data' => $updated_data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data PMI gagal diubah',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }

    function index_delete(){
        $id = $this->delete('id');
        $deleted_data = $this->Model_pmi->delete($id);
        
        if ($deleted_data) {
            $this->response([
                'status' => TRUE,
                'message' => 'Data PMI berhasil dihapus',
                'data' => $deleted_data

            ], REST_Controller::HTTP_OK);
        }else{
            $this->response([
                'status' => FALSE,
                'message' => 'Data PMI gagal dihapus',
                'data' => []
            ], REST_Controller::HTTP_OK); 
        }
    }

    

}