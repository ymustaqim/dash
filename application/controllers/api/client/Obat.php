<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Obat extends REST_Controller {

  function __construct($config = 'rest') {
    parent::__construct($config);
    $this->load->model('Model_obat');

  }

  function index_get() {
    $startDate  = $this->get('startDate');
    $endDate    = $this->get('endDate');
    $data       = $this->Model_obat->obat_data($startDate,$endDate);

    if ($data) {
      $this->response([
        'status' => TRUE,
        'message' => 'Data obat berhasil didapatkan',
        'data' => $data

      ], REST_Controller::HTTP_OK);
    }else{
      $this->response([
        'status' => FALSE,
        'message' => 'Data obat gagal didapatkan',
        'data' => []
      ], REST_Controller::HTTP_OK); 
    }
  }
}