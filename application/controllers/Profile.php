<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends CI_Controller {
	public function __construct(){
		parent::__construct();
		$this->load->model("Model_users");
		$this->load->helper('url');
	}

	public function index()
	{	
		$id=($this->session->userdata['logged_in']['user_id']);
		$data['user_info']=$this->Model_users->get($id);
		$data['user_info']=$data['user_info'][0];
		$this->load->view("apidocs/profile/index",$data);
	}

	
}
