<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_jenisinstansirelasi extends CI_model {

	//create fungsi untuk get data barang
	public function get_data_jenisinstansirelasi(){
		$query = $this->db->select('*')
		->from('jenis_instansi_relasi');
		return $query->get()->result();
	}

}