<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_instansirelasi extends CI_model {

	//create fungsi untuk get data barang
	public function get_data_instansirelasi(){
		$query = $this->db->select('*')
		->from('instansi_relasi');
		return $query->get()->result();
	}

}