<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_barang extends CI_model {

	//create fungsi untuk get data barang
	public function get_data_barang(){
		$query = $this->db->select('*')
		->from('barang');
		return $query->get()->result();
	}

	public function get_data_barang_detail($id){
		$query = $this->db->select('*')
		->from('barang')
		->where('id_barang',$id);
		return $query->get()->result();
	}

}