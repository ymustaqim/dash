<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_obat extends CI_model {

    public function obat_data($startDate,$endDate){
    	$startDate = $startDate . ' 00:00:00';
    	$endDate   = $endDate . ' 23:59:59';

		$where 	   = "ls.waktu BETWEEN '$startDate' AND '$endDate' ";

		$this->db->select('sum((-1*ls.selisih)) as jumlah , b.nama, ls.waktu')
		->from('log_stok ls')
		->join('jenis_transaksi jt','ls.id_transaksi=jt.id_jenis_transaksi')
		->join('stok s','s.id_stok=ls.id_stok')
		->join('packing_barang pb','s.id_packing_barang=pb.id_packing_barang')
		->join('barang b','pb.id_barang=b.id_barang')
		->join('sub_kategori_barang skb','b.id_sub_kategori_barang=skb.id_sub_kategori_barang')
		->where('jt.nama','Penjualan')
		->where('skb.nama','Obat')
		->where($where)
		->group_by(array('b.nama', 'ls.waktu'))
		->order_by('jumlah','desc');

		$query=$this->db->get();
		return $query->result();
    }
}