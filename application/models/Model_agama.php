<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_agama extends CI_model {

	//create fungsi untuk get data barang
	public function get_data_agama(){
		$query = $this->db->select('*')
		->from('agama');
		return $query->get()->result();
	}

	public function get_data_agama_detail($id){
		$query = $this->db->select('*')
		->from('agama')
		->where('id_agama',$id);
		return $query->get()->result();
	}

}