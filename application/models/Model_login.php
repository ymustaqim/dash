<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_login extends CI_model {

	public function login($data) {
		$username_input = $data['user_username'];
		$password_input = $data['user_password'];
		$check_username = $this->db->get_where('users',array('username' => $username_input))->first_row();
		if($check_username){
			$password_db = $check_username->password;
			if(password_verify($password_input,$password_db)){
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	public function read_user_information($username) {

		$condition = "username =" . "'" . $username . "'";
		$this->db->select('*');
		$this->db->from('users');
		$this->db->where($condition);
		$this->db->limit(1);
		$query = $this->db->get();

		if ($query->num_rows() == 1) {
			return $query->result();
		} else {
			return false;
		}
	}
}