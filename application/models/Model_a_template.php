<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_pmi extends CI_model {

	var $table = 'pmi'; 
	public function get($id = null)
	{
		if($id){
			$data = $this->db->get_where($this->table,array('id' => $id))->result_array();
			return $data;
		} else {
			$data = $this->db->get($this->table)->result_array();
			return $data;
		}
	}

	public function insert($data)
	{
		$this->db->insert($this->table,$data);
		$success = $this->db->affected_rows();
		if($success){
			$last_id = $this->db->insert_id();
			$inserted_data = $this->db->get_where($this->table,array('id' => $last_id))->result_array();
			return $inserted_data;
		} else {
			return false;
		}
	}
	
	public function update($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update($this->table,$data);
		
		$success = $this->db->affected_rows();
		if($success){
			$updated_data = $this->db->get_where($this->table, array('id' => $id))->result_array();
			return $updated_data;
		} else {
			return false;
		}
	}

	public function delete($id){
		$data = $this->db->get_where($this->table,array('id' => $id))->result_array();
		$delete = $this->db->delete($this->table, array('id' => $id));
		$success = $this->db->affected_rows();
		if($success){
			return $data;
		} else {
			return false;
		}
	}
	
}