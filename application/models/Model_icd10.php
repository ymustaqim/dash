<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_icd10 extends CI_model {

	//create fungsi untuk get data barang
	public function get_data_icd10(){
		$query = $this->db->select('*')
		->from('icd_10');
		return $query->get()->result();
	}

	public function get_data_icd10_detail($id){
		$query = $this->db->select('*')
		->from('icd_10')
		->where('id_icd_10',$id);
		return $query->get()->result();
	}

}