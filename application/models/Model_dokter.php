<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_dokter extends CI_model {

	public function get_data_dokter(){
		$query = $this->db->select('*')
		->from('dokter');
		return $query->get()->result();
	}

	public function get_data_dokter_detail($id){
		$query = $this->db->select('*')
		->from('dokter')
		->where('id_dokter',$id);
		return $query->get()->result();
	}
}