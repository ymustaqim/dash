<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Model_users extends CI_model {

	var $table = 'users';
	public function get($id = null)
	{
		if($id){
			$data_user = $this->db->get_where($this->table,array('id' => $id))->result_array();
			return $data_user;
		} else {
			$data_users = $this->db->get($this->table)->result_array();
			return $data_users;
		}
	}

	public function insert($data)
	{
		$this->db->insert($this->table,$data);
		$success = $this->db->affected_rows();
		if($success){
			$last_id = $this->db->insert_id();
			$inserted_data = $this->db->get_where($this->table,array('id' => $last_id))->result_array();
			return $inserted_data;
		} else {
			return false;
		}
	}
	
	public function update($id, $data)
	{
		$this->db->where('id',$id);
		$this->db->update($this->table,$data);
		
		$success = $this->db->affected_rows();
		if($success){
			$updated_data = $this->db->get_where($this->table, array('id' => $id))->result_array();
			return $updated_data;
		} else {
			return false;
		}
	}

	public function delete($id){
		$data = $this->db->get_where($this->table,array('id' => $id))->result_array();
		$delete = $this->db->delete($this->table, array('id' => $id));
		$success = $this->db->affected_rows();
		if($success){
			return $data;
		} else {
			return false;
		}
	}

	public function get_valid_login(){
		$users = $this->db->get($this->table)->result_array();

		$list_valid_login = [];
		foreach ($users as $key => $user) {
			$data = [$user['username'] => $user['password']];
			$list_valid_login = array_merge($list_valid_login, $data);
		}
		return $list_valid_login;
	}
	
}